# bbook

bbook is a terminal-oriented contact management utility that allows
you to log interactions with your contacts.

## Requirements

* A recent version of bash. (Written using 4.3)
* GNU Recutils
* GNU coreutils

## Installing

Put the bbook file somewhere in your path and make it
executable, e.g. ~/.local/bin/.  If readrec.so is not under
/usr/lib/recutils/bash-builtins/, you will need to find it and update
the readrec_path in the script.  The first time you run bbook, it will
create empty files to hold your contacts and notes in ~/.bbook/.

## Functions

Contact Functions:

**n** - Add a note to the current contact.  
**e** - Edit the current contact.
**f** - Toggle followup flag.  
**h** - Show previous contact in the list.  
**j** - Select the next note in the list.  
**l** - Show next contact in the list.  
**k** - Select the previous note in the list.  
**a** - Toggle status active/inactive.  
**u** - Show previous page of notes.  
**d** - Show next page of notes.  
**Enter** - View the selected note. \<Space\> works, too.  

Global Functions:

**N** - Add a contact.  
**F** - Toggle followup list. Show only contacts with followup flag set.  
**A** - Toggle active filter. Show only contacts with Status = active.
**/** - Search. Enter a blank value to clear a search.  
**q** - Exit bbook.  
**?** - Show help.  

## Backlog

* Set follow-up date on a contact
* User-definable saved-lists
* track opportunities as a distinct thing?
  * Contact FK
  * Company
  * Location
  * Term Length
  * Start Date
  * Title
  * Rate
  * Notes
* mutt integration ala abook
* Local and system address books?
* User-definable fields?
